#!bin/env python2.7

"""
This is the test script for Homework 4, Task 1 (ling571, Winter 2013).
original author: Scott Farrar
modified 2013: Juie Medero
"""
from nltk.sem import LogicParser

lp = LogicParser() # Create a logical parser

# Open the file to write results to
outfile = open('output.fol','w')

#process each line in formulas.fol (the file you will write)
for line in open('formulas.fol'):
    # Skip comments and blank lines
    if line.startswith('#') or line.isspace(): continue 
    # Attempt to parse
    try: print >> outfile, lp.parse(line.strip())
    except: print >> outfile, 'FAILED! -->', line.strip()

outfile.close()

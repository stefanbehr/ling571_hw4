#!/bin/env python2.7

"""
This is the test script for Homework 4, Task 2 (ling571, Winter 2013).
original author: Scott Farrar
modified 2013: Julie Medero
"""

from nltk.sem import LogicParser
from nltk.inference import Prover9Command

lp = LogicParser() # Create a logic parser

# Open the file to write results to
outfile = open('output.prover','w')

foundGoal = False
formulas=[]

# Process the content of formulas.prover (which you will write)
for line in open('formulas.prover'):
    line=line.strip()
    # Skip blank lines and comments
    if not(line) or line.startswith("#"): continue

    if line == "GOAL": foundGoal=True        
    else:
        try: thisFormula = lp.parse(line)
        except Exception as e:
            thisFormula=''
            print >> outfile, 'Failed! --> %s %s'  % (line, e)
        if foundGoal:# Call Prover9
            print >> outfile, Prover9Command(thisFormula, formulas).prove()
            formulas=[]
            foundGoal = False
        else: formulas.append(thisFormula)

outfile.close()
